# I think it's a good idea for you to experiment with sets since they're a very useful part of Python.
# Start by creating a set variable named songs that has three song titles in it. You can use any titles you want, just make sure they're three different strings.

songs = {"now", "love", "then"}


# Awesome. Now use the .add() method to add the title "Treehouse Hula" to songs.

songs.add("Treehouse Hula")


# Alright, and last task. Use .update() to add the following two sets to your songs set.
# {"Python Two-Step", "Ruby Rhumba"}
# {"My PDF Files"}

songs.update({"Python Two-Step", "Ruby Rhumba"}, {"My PDF Files"})

