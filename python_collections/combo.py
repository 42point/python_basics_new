Create a function named combo that takes two ordered iterables. These could be tuples, lists, strings, whatever.
Your function should return a list of tuples. Each tuple should hold the first item in each iterable, then the second set, then the third, and so on. Assume the iterables will be the same length.
Check the code below for an example.


# combo([1, 2, 3], 'abc')
# Output:
# [(1, 'a'), (2, 'b'), (3, 'c')]

def combo(first, second):
	result_list = []
	for item in range(len(first)):
		my_t = first[item], second[item]
		result_list.append(my_t)
	return result_list