def num_teachers(teachers):
	how_many = 0
	for i in teachers.keys():
		how_many +=1
	return how_many

def num_courses(teachers):
	courses_count = 0
	for i in teachers.values():
		courses_count += len(i)
	return courses_count
	
def courses(teachers):
	courses_list = []
	for i in teachers.values():
		courses_list.append(i)
	return courses_list
	
def most_courses(teachers):
	max_count = 0
	most_busy = ""
	for i in teachers:
		if len(teachers[i]) > max_count:
			max_count = len(teachers[i])
			most_busy = i
	return most_busy
	
	
def stats(teachers):
	new_list = []
	temp_list = []
	for number in teachers:
		temp_list = [number, len(teachers[number])]
		new_list.append(temp_list)
	return new_list
	
teachers = {'Andrew Chalkley': ['jQuery Basics','Node.js Basics'],'Kenneth Love': ['Python Basics', 'Python Collections']}
print(most_courses(teachers))
